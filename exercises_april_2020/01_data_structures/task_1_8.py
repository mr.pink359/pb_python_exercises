# -*- coding: utf-8 -*-
'''
Задание 1.8

Дано:
student - кол-во школьников
apple - кол-во яблок

Школьники делят между собой  поровну яблоки из корзины
Оставшиеся яблоки остаются в корзине
Подсчитать сколько яблок достанется одному школьнику

Вариант для продвинутых:
Дополнительно написать код, что число школьников 
и яблок - задается с клавиатуры пользователем
'''


student = int(input("Please enter the number of students:\n"))
apple = int(input("Please enter the number of apples in a basket:\n"))
count = apple // student
leftover = apple % student
print(f'Every student got {count} apple(s) from a basket\nThere is {leftover} apple(s) left in a basket')
