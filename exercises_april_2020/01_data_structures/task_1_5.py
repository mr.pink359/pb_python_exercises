# -*- coding: utf-8 -*-
'''
Задание 1.5

Из строк pages1 и pages2 получить список страниц,
которые есть и в pages1  и в  pages2.

Результатом должен быть список: ['1', '3', '8']

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

pages1 = 'интересные страницы: 1,2,3,5,8'
pages2 = 'интересные страницы: 1,3,8,9'

last_space_index = pages1.rfind(' ')
pages1 = pages1[last_space_index + 1:]
pages1 = pages1.split(',')

last_space_index = pages2.rfind(' ')
pages2 = pages2[last_space_index+1:]
pages2 = pages2.split(',')

mutual_pages_list = []
for i in pages1:
    if pages2.count(i) > 0:
        mutual_pages_list = mutual_pages_list + [i]
print(mutual_pages_list)
