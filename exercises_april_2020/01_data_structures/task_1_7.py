# -*- coding: utf-8 -*-
'''
Задание 1.7

Дано число n

Подсчитать и вывесети на экран сумму эго цифр

Вариант для продвинутых:
Дополнительно написать код, что 5ти значное число n - задается с клавиатуры пользователем
'''


n = input("Please enter any integer value:\n")
n = int(n)
multiplier = 10
my_sum = 0
while n > 0:
    my_sum += n % multiplier
    n = n // multiplier
print(f"Sum of your value's digits is: {my_sum}")
